# Introduction
Hello, this is my first project in which I will share the exercises I have done.

I started learning programming with python by watching two courses:
1. [Learn Python - Full Course for Beginners [Tutorial]](https://www.youtube.com/watch?v=rfscVS0vtbw)
2. [Automate the Boring Stuff with Python](https://www.youtube.com/playlist?list=PL0-84-yl1fUnRuXGFe_F7qSH1LEnn9LkW)

After the theoretical part...

## Done (in order)
### my_first_game.py

I wrote a simple game in which you have to guess a noun of 4 letters. After each try, you find out how many characters you have guessed (the place of the letter matters).
example:
secret word = head,
your guess = hand,
number of guessed characters = 2.

The "a" in the word head and hand is in a different place - does not count as guessed

### range_X.py function 
I wrote a function that will return a list of numbers from a given range, with a given step in any direction.

This function works similar to the built-in "range" function


### Practice Python with www.practicepython.org
#### finished

- [01: Character Input](https://www.practicepython.org/exercise/2014/01/29/01-character-input.html)
- [02: Odd Or Even](https://www.practicepython.org/exercise/2014/02/05/02-odd-or-even.html)
- [03: List Less Than Ten](https://www.practicepython.org/exercise/2014/02/15/03-list-less-than-ten.html)
- [04: Divisors](https://www.practicepython.org/exercise/2014/02/26/04-divisors.html)
- [05: List Overlap](https://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html)
- [06: String Lists](https://www.practicepython.org/exercise/2014/03/12/06-string-lists.html)
- [07: List Comprehensions](https://www.practicepython.org/exercise/2014/03/19/07-list-comprehensions.html)
- [08: Rock Paper Scissors](https://www.practicepython.org/exercise/2014/03/26/08-rock-paper-scissors.html)
- [09: Guessing Game One](https://www.practicepython.org/exercise/2014/04/02/09-guessing-game-one.html)
- [10: List Overlap Comprehensions](https://www.practicepython.org/exercise/2014/04/10/10-list-overlap-comprehensions.html)
- [11: Check Primality Functions](https://www.practicepython.org/exercise/2014/04/16/11-check-primality-functions.html)
- [12: List Ends](https://www.practicepython.org/solution/2014/05/15/12-list-ends-solutions.html)
- [13: Fibonacci](https://www.practicepython.org/exercise/2014/04/30/13-fibonacci.html)
- [14: List Remove Duplicates](https://www.practicepython.org/exercise/2014/05/15/14-list-remove-duplicates.html)
- [15: Reverse Word Order](https://www.practicepython.org/exercise/2014/05/21/15-reverse-word-order.html)
- [16: Password Generator](https://www.practicepython.org/exercise/2014/05/28/16-password-generator.html)
- [17: Decode A Web Page](https://www.practicepython.org/exercise/2014/06/06/17-decode-a-web-page.html)
- [18: Cows And Bulls](https://www.practicepython.org/exercise/2014/07/05/18-cows-and-bulls.html)
- [19: Decode A Web Page Two](https://www.practicepython.org/exercise/2014/07/14/19-decode-a-web-page-two.html)
- [20: Element Search](https://www.practicepython.org/exercise/2014/11/11/20-element-search.html)
- [21: Write To A File](https://www.practicepython.org/exercise/2014/11/30/21-write-to-a-file.html)
- [22: Read From File](https://www.practicepython.org/exercise/2014/12/06/22-read-from-file.html)
- [23: File Overlap](https://www.practicepython.org/exercise/2014/12/14/23-file-overlap.html)
- [24: Draw A Game Board](https://www.practicepython.org/exercise/2014/12/27/24-draw-a-game-board.html)
- [25: Guessing Game Two](https://www.practicepython.org/exercise/2015/11/01/25-guessing-game-two.html)

#### In the queue
- 26: Check Tic Tac Toe
- 27: Tic Tac Toe Draw
- 28: Max Of Three
- 29: Tic Tac Toe Game
- 30: Pick Word
- 31: Guess Letters
- 32: Hangman
- 33: Birthday Dictionaries
- 34: Birthday Json
- 35: Birthday Months
- 36: Birthday Plots 

## Environment

- Python 3.8
- PyCharm 2020.2 (Community Edition)
- Windows 7 (Home Premium)