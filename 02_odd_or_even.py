# https://www.practicepython.org/exercise/2014/02/05/02-odd-or-even.html
# Ask the user for a number.
# Depending on whether the number is even or odd, print out an appropriate message to the user.
# #Hint: how does an even / odd number react differently when divided by 2?

number = int(input("Please type some number: "))

if number % 2 == 0:
    print("You typed a even number!")
else:
    print("You typed a odd number!")

#Extras:
# If the number is a multiple of 4, print out a different message.
if number % 4 == 0:
    print("You typed a number divisible by 4!")

# Ask the user for two numbers:
# one number to check (call it num)
# and one number to divide by (check).
# If check divides evenly into num, tell that to the user.
# If not, print a different appropriate message.

anoter_number_1 = int(input("Please type secound number: "))
anoter_number_2 = int(input("Please type third number: "))

if anoter_number_1 % anoter_number_2 == 0:
    print("You can divide " + str(anoter_number_1) + " by " + str(anoter_number_2) + " without modulo!")
else:
    print("You can't divide " + str(anoter_number_1) + " by " + str(anoter_number_2) + " without modulo!")