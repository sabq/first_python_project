# https://www.practicepython.org/exercise/2014/05/15/14-list-remove-duplicates.html
# Write a program (function!) that takes a list and returns a new list that contains all the elements of the first list
# minus all the duplicates.
# Extras:
# Write two different functions to do this - one using a loop and constructing a list, and another using sets.
# Go back and do Exercise 5 using sets, and write the solution for that in a different function.
import random

def list_without_duplicates(a):
    new_list = []
    for number in a:
        if number not in new_list:
            new_list.append(number)
    return new_list

def list_set_list(a):
    return list(set(a))

random_list_size = random.randint(4,20)
random_list = [random.randint(0,25) for num in range(random_list_size)]
random_list.sort()
print(random_list)

print("make by for loop: " + str(list_without_duplicates(random_list)))
print("make by set func: " + str(list_set_list(random_list)))