# https://www.practicepython.org/exercise/2014/12/27/24-draw-a-game-board.html

'''
This exercise is Part 1 of 4 of the Tic Tac Toe exercise series.
The other exercises are: Part 2, Part 3, and Part 4.
Time for some fake graphics! Let’s say we want to draw game boards that look like this:
 --- --- ---
|   |   |   |
 --- --- ---
|   |   |   |
 --- --- ---
|   |   |   |
 --- --- ---
This one is 3x3 (like in tic tac toe).
Obviously, they come in many other sizes (8x8 for chess, 19x19 for Go, and many more).
Ask the user what size game board they want to draw, and draw it for them to the screen using Python’s print statement.
Remember that in Python 3, printing to the screen is accomplished by
print("Thing to show on screen")
Hint: this requires some use of functions, as were discussed previously on this blog and elsewhere on the Internet,
like this TutorialsPoint link.
'''

def make_game_board(_size):
    _table = ""
    for i in range(_size):
        _table = _table + " ---"*_size + " \n"
        _table = _table + "|   "*_size + "|\n"
    _table = _table + " ---"*_size + " \n"
    return(_table)

print("hello, I will print game board to you.")
_size_of_board = input("please choose number of rows and columns in range from 3 to 8: ")
_size_of_board = int(_size_of_board)

if _size_of_board >=3 and _size_of_board <= 8:
    print(f"\nYou choose size of game board {_size_of_board} x {_size_of_board}")
    print("Please!\n")
else:
    print("\nYou didn't choose correct number! bye, bye.")
    quit()

_game_board = make_game_board(_size_of_board)
print(_game_board)

# bellow - nice solusions from https://www.practicepython.org/
'''
1:
a = '---'.join('        ')
b = '   '.join('||||||||')
print('\n'.join((a, b, a, b, a, b, a, b, a, b, a, b, a, b, a)))

2:
def drawboard(kamal):
    kamal = int(kamal)
    i = 0
    ho = "--- "
    ve = "|   "
    ho = ho * kamal
    ve = ve * (kamal+1)
    while i < kamal+1:
        print ho
        if not (i == kamal):
            print ve
        i += 1
'''