# - [20: Element Search](https://www.practicepython.org/exercise/2014/11/11/20-element-search.html)
# Write a function that takes an ordered list of numbers
# (a list where the elements are in order from smallest to largest) and another number.
# The function decides whether or not the given number is inside the list and returns
# (then prints) an appropriate boolean.
# Extras:
# Use binary search.
import random

def make_ordered_list(min_length, max_length):
    ordered_list_length = random.randint(min_length,max_length)
    list_of_numbers = []
    ordered_list = []
    for i in range(0,1000):
        list_of_numbers.append(i)
    for i in range(ordered_list_length):
        drawed_number = random.sample(list_of_numbers,1)
        #drawed_number = random.sample(list_of_numbers,1)
        list_of_numbers.remove(drawed_number[0])
        ordered_list.append(drawed_number[0])
        ordered_list.sort()
    return ordered_list

def get_number_to_check():
    x = input("Please type o number in range 0 to 999: ")
    return x

def check_is_number_in_list(number, list):
    if number in list:
        return True
    else:
        return False

def check_is_number_in_list_2(number, list):
    while True:
        if len(list) == 1:
            if list[0] == number:
                return True
                break
            else:
                return False
                break
        else:
            middle = int(len(list) / 2)
            if list[middle] == number:
                return True
            elif list[middle] > number:
                list = list[:middle]
            elif list[middle] < number:
                list = list[(middle):]

list = make_ordered_list(10,1000)
print("list:", list)
number_to_check = int(get_number_to_check())
print("number_to_check:", number_to_check)
print("check_is_number_in_list:", check_is_number_in_list(number_to_check, list))
print("check_is_number_in_list_2:", check_is_number_in_list_2(number_to_check, list))
