# https://www.practicepython.org/exercise/2014/03/26/08-rock-paper-scissors.html
# Make a two-player Rock-Paper-Scissors game. (Hint: Ask for player plays (using input), compare them, print out a message of congratulations to the winner, and ask if the players want to start a new game)

#from .display import clear_output
print("Hello, let's play some game.")
print("You will fight a duel on Rock-Paper-Scissors game.")
print("It is game for two players.")
player_one_name = input("\nPlayer one, could you please type your name: ")
player_one_score = 0;
player_two_name = input("\nPlayer two, could you please type your name: ")
player_two_score = 0;

#Remember the rules:
# Rock beats scissors
# Scissors beats paper
# Paper beats rock
while True:
#    clear_output(wait=True)
    player_one_choice = input("rock, scissors or paper. " + player_one_name + " please choose: ")
#    clear_output(wait=True)
    player_two_choice = input("rock, scissors or paper. " + player_two_name + " please choose: ")
#    clear_output(wait=True)
    if player_one_choice == "quit" or player_two_choice == "quit":
        break
    if (player_one_choice == "rock" and player_two_choice == "scissors") or (player_one_choice == "scissors" and player_two_choice == "paper") or (player_one_choice == "paper" and player_two_choice == "rock"):
        player_one_score = 1
    elif (player_two_choice == "rock" and player_one_choice == "scissors") or (player_two_choice == "scissors" and player_one_choice == "paper") or (player_two_choice == "paper" and player_one_choice == "rock"):
        player_two_score = 1
    if player_one_score == 1:
        print("\n   " + player_one_name.upper() + " YOU WIN!")
    elif player_two_score == 1:
        print("\n   " + player_two_name.upper() + " YOU WIN!")
    else:
        print("\n   YOU DRAW!")
    new_game = input("\nDo you want to play again (yes/no): ")
    if new_game == "yes":
        player_one_score = 0
        player_two_score = 0
    elif new_game == "no":
        break
    else:
        print("you do not type yes or no / game quit")
        break
