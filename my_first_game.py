import random

#say hello and give short info about game
print("hello! What is your name?")
name = input()
print("Well, " + name + ", let's play game!")
print("\nAll you need is to guess the 'secret word' :)")
print("The 'secret word' is build with four letters.")
print("Good luck!\n")
print("tip: secret word is in polish language.\n")


#list of words we need to guess
list_of_words  = ["bark", "biga", "buda", "czad", "dług", "druk",
                  "duch", "dżem", "fort", "garb", "gips", "gofr",
                  "grot", "hala", "izba", "kark", "kask",
                  "kępa", "kiła", "kino", "kiwi", "klej", "kmin", "kosz", "koza", "kruk", "kura",
                  "lipa", "list", "lont", "lupa", "łąka", "łata",
                  "mata", "meta", "noga", "ogon", "okno", "pęto", "pies", "piwo", "płód",
                  "pług", "płyn", "ptak", "puma", "rafa",
                  "rdza", "ring", "rosa", "rtęć", "rufa", "ryba", "rzez", "sień", "słój",
                  "słup", "smog", "soja", "sowa", "srom",
                  "staw", "ster", "stół", "szef", "szew", "tlen", "torf",
                  "trup", "usyp", "waga", "wilk", "wino", "wlot", "właz",
                  "włos", "woda", "wzór", "ziew", "znak", "zręb",
                  "zysk", "żaba", "żłób", "znak", "żółć", "żółw", "żyto" ]

#function made to check is it list is sorted from a...z
def check_if_list_sorted():
    list_of_words1 = list_of_words
    list_of_words1.sort()

    for i in range(len(list_of_words)):
        print(list_of_words[i] == list_of_words1[i])

#pick up random word to play a game
random_int = random.randint(1,len(list_of_words))
random_word = list_of_words[random_int - 1]
#print("debug, nr z listy: " + str(random_int))
#print("debug, słowo z listy: " + random_word)

#game
while 1 == 1:
    typed_word = input("guess the word: ")
    guesed_chars = 0
    if len(random_word) == len(typed_word):
        for i in range(len(random_word)):
            #print(random_word[i] + " / " + typed_word[i])
            if random_word[i] == typed_word[i]:
                guesed_chars += 1
        if guesed_chars == 4:
            break
        else:
            print("liczba odgadniętych znaków: " + str(guesed_chars))
    else:
        print("please, type only 4 letters.")


#final information
print("\nWOW! You gessed te word!")