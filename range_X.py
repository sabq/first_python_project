'''
This is alternative to range function
I make this function only for one reason - practice :)

First parameter: start;  (if you type secound parameter) /or/ stop (if you don't type secound parameter)
Secound parameter: stop;  default False => start = 0 /and/ first parameter = range
Third parameter: step;  default False => step = 1
Fourth parameter: reverse;  default False

If you type one parameter - function will make list from 0 to your parameter minus one
If you type two parameters - function will make list from first parameter to your secound parameter minus one
If you type third parameter - function will make list from first parameter to your secound parameter minus one with step equal to third parameter
If you type fourth parameter equal True - function will make list in reverse order

have fun :)
'''

def range_sabq(start: int, stop: int = False, step: int = False, reverse = False):
    range_container = []

    if stop == False:
        first_number = 0
        last_number = start
        # print('stop = False')
        # print('first_number = {}'.format(first_number))
        # print('last_number = {}'.format(last_number))
    else:
        first_number = start
        last_number = stop
        # print('stop = True')
        # print('first_number = {}'.format(first_number))
        # print('last_number = {}'.format(last_number))

    if reverse == True:
        # print('reverse == True')
        i = last_number
        while i > first_number:
            i -= 1
            if step == False:
                # print('step = False')
                range_container.append(i)
            else:
                # print('step = True')
                filter = (last_number - 1) % step
                if (i % step) == filter:
                    range_container.append(i)
    else:
        # print('reverse == False')
        i = first_number
        while i < last_number:
            if step == False:
                # print('step = False')
                range_container.append(i)
            else:
                # print('step = True')
                filter = first_number % step
                if (i % step) == filter:
                    range_container.append(i)
            i += 1

    # print(range_container)
    return(range_container)

# examples
print(range_sabq(15))
print(range_sabq(6, 17))
print(range_sabq(-4, 32, 6))
print(range_sabq(-4, 33, 6, True))
print(range_sabq(start=-36, stop=48, step= 13, reverse=True))