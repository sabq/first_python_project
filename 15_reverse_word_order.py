# https://www.practicepython.org/exercise/2014/05/21/15-reverse-word-order.html
# Write a program (using functions!) that asks the user for a long string containing multiple words.
# Print back to the user the same string, except with the words in backwards order. For example, say I type the string:
# My name is Michele
# Then I would see the string:
# Michele is name My
# shown back to me.

def reverse_string(text_to_reverse):
    text_to_reverse_list = text_to_reverse.split(" ")
    text_to_reverse_list.reverse()
    text_to_reverse = " ".join(text_to_reverse_list)
    return text_to_reverse


long_text = input("Please type some long text to reverse it:\n")
print("\nHere is it:\n" + reverse_string(long_text))