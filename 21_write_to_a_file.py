# https://www.practicepython.org/exercise/2014/11/30/21-write-to-a-file.html
# Take the code from the How To Decode A Website exercise
# (if you didn’t do it or just want to play with some different code, use the code from the solution),
# and instead of printing the results to a screen, write the results to a txt file.
# In your code, just make up a name for the file you are saving to.
# Extras:
# Ask the user to specify the name of the output file that will be saved.

import requests
from bs4 import BeautifulSoup

url = "https://www.nytimes.com/"
#print("url\n" + url)
r = requests.get(url)
#print("r\n", r)
r_html = r.text
#print("r_html\n" + r_html)
soup = BeautifulSoup(r_html, 'html.parser')
title = soup.find_all('h2')
all_titles = ""
for number in range(len(title)):
    all_titles += title[number].string + "\n"

with open('titles.txt', 'w') as open_file:
    open_file.write(all_titles)