# https://www.practicepython.org/exercise/2014/04/30/13-fibonacci.html
# Write a program that asks the user how many Fibonnaci numbers to generate and then generates them.
# Take this opportunity to think about how you can use functions.
# Make sure to ask the user to enter the number of numbers in the sequence to generate.
# (Hint: The Fibonnaci seqence is a sequence of numbers where the next number in the sequence
# is the sum of the previous two numbers in the sequence. The sequence looks like this: 1, 1, 2, 3, 5, 8, 13, …)
print("hello!")
print("I will write Fibonacci numbers as many as you want.")
how_many_fibonacci_numbers = input("Please type how many Fibonacci numbers you want to see: ")
Fibonacci_number_list = [0,1]
for number in range(3, (int(how_many_fibonacci_numbers))):
    Fibonacci_number_list.append(Fibonacci_number_list[number - 2] + Fibonacci_number_list[number - 3])
print(Fibonacci_number_list)