# https://www.practicepython.org/exercise/2014/12/06/22-read-from-file.html
#Given a .txt file that has a list of a bunch of names, count how many of each name there are in the file,
# and print out the results to the screen. I have a .txt file for you, if you want to use it!
# Extra:
# Instead of using the .txt file from above (or instead of, if you want the challenge),
# take this .txt file, and count how many of each “category” of each image there are.
# This text file is actually a list of files corresponding to the SUN database scene recognition database,
# and lists the file directory hierarchy for the images.
# Once you take a look at the first line or two of the file, it will be clear which part represents the scene category.
# To do this, you’re going to have to remember a bit about string parsing in Python 3.
# I talked a little bit about it in this post.


'''
Exercise bellow:

with open('nameslist.txt', 'r') as open_file:
    _text_lines = open_file.readlines()

for i in range(len(_text_lines)):
    print(f'{i} {_text_lines[i]}')
'''

# Extra bellow:

with open('Training_01.txt', 'r') as open_file:
    _category_lines = open_file.readlines()

_categories = []

for i in range(len(_category_lines)):
    _first_char_position = _category_lines[i].find("/",1)+1
    _last_char_position = _category_lines[i].find("/",_category_lines[i].find("/",1)+1)
    _category_lines[i] = _category_lines[i][_first_char_position:+_last_char_position]
    if _category_lines[i] not in _categories:
        _categories.append(_category_lines[i])

print(_categories)
