# https://www.practicepython.org/exercise/2014/03/05/05-list-overlap.html
# Take two lists, say for example these two:
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
# and write a program that returns a list that contains only the elements that are common between the lists
# (without duplicates). Make sure your program works on two lists of different sizes.
import random

a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
c = []
for element in a:
    if element in a and element in b:
        if element not in c:
            c.append(element)
print("Make in few lines " + str(c))

# Extras:
# 1. Randomly generate two lists to test this
# 2. Write this in one line of Python (don’t worry if you can’t figure this out at this point - we’ll get to it soon)
random_list_one_size = random.randint(8, 20)
random_list_two_size = random.randint(8, 20)
random_list_one = [random.randint(1,99) for num in range(random_list_one_size)]
random_list_two = [random.randint(1,99) for num in range(random_list_two_size)]

#print(list(set(num for num in random_list_one if num in random_list_two)))
print("Make in 1 line: " + str(list(set(num for num in a if num in b))))
