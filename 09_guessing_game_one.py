# https://www.practicepython.org/exercise/2014/04/02/09-guessing-game-one.html
# Generate a random number between 1 and 9 (including 1 and 9).
# Ask the user to guess the number, then tell them whether they guessed too low, too high, or exactly right.
# (Hint: remember to use the user input lessons from the very first exercise)
# Extras:
# Keep the game going until the user types “exit”
# Keep track of how many guesses the user has taken, and when the game ends, print this out.
import random
lower_limit, upper_limit = 1, 20
random_number = random.randint(lower_limit, upper_limit)
how_many_guesses = 0;

print("Hello, let's play some game.")
print("You need to guess number between " + str(lower_limit) + " and " + str(upper_limit) + ".")
player_name = input("Could you please type your name: ")
print(player_name + " if you want finish just type 'quit'")

while True:
    your_guess = input("guess te number : ")
    how_many_guesses += 1
    if your_guess == "quit":
        break
    elif int(your_guess) > random_number:
        print("to high")
    elif int(your_guess) < random_number:
        print("to low")
    elif int(your_guess) == random_number:
        print("\n" + player_name + " you WIN!")
        print("You needed only " + str(how_many_guesses) + " guesses.")
        print("\nLets play again!")
        random_number = random.randint(lower_limit, upper_limit)
        how_many_guesses = 0
