# https://www.practicepython.org/exercise/2014/03/12/06-string-lists.html
# Ask the user for a string and print out whether this string is a palindrome or not.
# (A palindrome is a string that reads the same forwards and backwards.)
string = input("Please write some word to check if it is palindrome: ")
is_a_palindrome = 1

for i in range(len(string)):
    if string[i] != string[- ( i + 1)]:
        is_a_palindrome = 0

if is_a_palindrome == 1:
    print(string + " is a palindrome")
else:
    print(string + " is not a palindrome")