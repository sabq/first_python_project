# https://www.practicepython.org/exercise/2014/04/10/10-list-overlap-comprehensions.html
# This week’s exercise is going to be revisiting an old exercise (see Exercise 5),
# except require the solution in a different way.
# Take two lists, say for example these two:
#	a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
#	b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
import random
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# and write a program that returns a list that contains only the elements that are common between the lists (without duplicates).
# Make sure your program works on two lists of different sizes.
c = []
for element in a:
    if element in b and element not in c:
        c.append(element)
print(c)

# Extra:
# Randomly generate two lists to test this

random_list_one_size = random.randint(8, 20)
random_list_two_size = random.randint(8, 20)
random_list_one = [random.randint(1,99) for num in range(random_list_one_size)]
random_list_two = [random.randint(1,99) for num in range(random_list_two_size)]
print(random_list_one)
print(random_list_two)

d = []
for element in random_list_one:
    if element in random_list_two and element not in d:
        d.append(element)
print(d)

# Write this in one line of Python using at least one list comprehension. (Hint: Remember list comprehensions from Exercise 7).
# The original formulation of this exercise said to write the solution using one line of Python,
# but a few readers pointed out that this was impossible to do without using sets that I had not yet discussed on the blog,
# so you can either choose to use the original directive and read about the set command in Python 3.3,
# or try to implement this on your own and use at least one list comprehension in the solution.