# https://www.practicepython.org/exercise/2014/05/28/16-password-generator.html
# Write a password generator in Python. Be creative with how you generate passwords
# strong passwords have a mix of lowercase letters, uppercase letters, numbers, and symbols.
# The passwords should be random, generating a new password every time the user asks for a new password.
# Include your run-time code in a main method.
# Extra:
# Ask the user how strong they want their password to be. For weak passwords, pick a word or two from a list.
# sabq - [A, a, B, b, C, c, D, d, E, e, F, f, G, g, H, h, I, i, J, j, K, k, L, l, M, m, N, n, O, o, P, p, Q, q, R, r, S, s, T, t, U, u, V, v, W, w, X, x, Y, y, Z, z]
import random
print("I will generate password to you.")
print("You can choose between easy/medium/hard password")
print("easy password have eight letters")
print("medium password have ten letters and numbers")
print("strong password have twelve letters, numbers and symbols")
how_strong = input("Please, type how strong password you want: ")

small_chars_allowed_in_passowrd = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
large_chars_allowed_in_passowrd = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
numbers_allowed_in_passowrd = ["0", "1", "2", "3", "4", "5", "6", "7", "8" "9"]
special_chars_allowed_in_passowrd = ["!", "@", "$", "%", "^", "&", "*", "(", ")", "{", "}", ":", "<", ">"]

if how_strong == "easy":
    print("".join(random.sample(small_chars_allowed_in_passowrd + large_chars_allowed_in_passowrd, 8)))
elif how_strong == "medium":
    print("".join(random.sample(small_chars_allowed_in_passowrd + large_chars_allowed_in_passowrd + numbers_allowed_in_passowrd, 10)))
elif how_strong == "strong":
    print("".join(random.sample(small_chars_allowed_in_passowrd + large_chars_allowed_in_passowrd + numbers_allowed_in_passowrd + special_chars_allowed_in_passowrd, 12)))
else:
    print("You wasn't type 'easy', 'medium' or 'hard'!")