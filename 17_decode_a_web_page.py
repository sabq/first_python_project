# https://www.practicepython.org/exercise/2014/06/06/17-decode-a-web-page.html
# Use the BeautifulSoup and requests Python packages to print out a list of all the article titles
# on the New York Times homepage.
import requests
from bs4 import BeautifulSoup

url = "https://www.nytimes.com/"
#print("url\n" + url)
r = requests.get(url)
#print("r\n", r)
r_html = r.text
#print("r_html\n" + r_html)
soup = BeautifulSoup(r_html, 'html.parser')
title = soup.find_all('h2')
for number in range(len(title)):
    print(title[number].string)