# https://www.practicepython.org/exercise/2014/01/29/01-character-input.html
# Create a program that asks the user to enter their name and their age.

print("Hi. What's your name?")
name = input()
print(name + " what is your age?")
age = int(input())

# Print out a message addressed to them that tells them the year that they will turn 100 years old.
current_year = 2020
age_100 = "Oh, well. You will be 100 years old in the year " + str(current_year + (100 - age)) + "."
print(age_100)

# Extras:
# Add on to the previous program by asking the user for another number
# and printing out that many copies of the previous message.
# (Hint: order of operations exists in Python)

number = int(input("\nPlease type some number: "))
print(age_100 * number)

# Print out that many copies of the previous message on separate lines.
# (Hint: the string "\n is the same as pressing the ENTER button)

number = int(input("\nPlease type some number: "))
print( ("\n" + age_100) * number)